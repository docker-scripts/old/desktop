include(bionic)

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

### unminimize
RUN yes | unminimize

### add LinuxMint repository
RUN apt install --yes gnupg
COPY misc/A6616109451BBBF2.pubkey /tmp/
RUN apt-key add /tmp/A6616109451BBBF2.pubkey
RUN echo "deb http://packages.linuxmint.com tricia main upstream import backport" \
    > /etc/apt/sources.list.d/linuxmint.list

### setup apt preferences
RUN echo "\
Package: *\n\
Pin: origin build.linuxmint.com\n\
Pin-Priority: 700\
" > /etc/apt/preferences.d/official-extra-repositories.pref

RUN echo "\
Package: *\n\
Pin: origin live.linuxmint.com\n\
Pin-Priority: 750\n\
\n\
Package: *\n\
Pin: release o=linuxmint,c=upstream\n\
Pin-Priority: 700\n\
\n\
Package: *\n\
Pin: release o=Ubuntu\n\
Pin-Priority: 500\n\
" > /etc/apt/preferences.d/official-package-repositories.pref

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

### install the desktop and display manager
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes -o Dpkg::Options::=--force-confnew \
        lightdm lightdm-settings slick-greeter \
        mint-meta-mate
RUN systemctl set-default graphical.target

### install x2go server
RUN apt install --yes x2goserver

### install Guacamole dependences
RUN apt install --yes \
        build-essential libcairo2-dev libjpeg-turbo8-dev libpng-dev libtool-bin libossp-uuid-dev
RUN apt install --yes \
        libavcodec-dev libavutil-dev libswscale-dev \
        freerdp2-dev libpango1.0-dev libssh2-1-dev libtelnet-dev libvncserver-dev libwebsockets-dev \
        libpulse-dev libssl-dev libvorbis-dev libwebp-dev
RUN apt install --yes \
        dpkg-dev tomcat9 apache2 git \
        xrdp xorgxrdp xrdp-pulseaudio-installer pulseaudio

ENV VER=1.1.0
ENV GUAC_ARCHIVE=http://archive.apache.org/dist/guacamole/$VER

### install guacamole server
RUN wget "$GUAC_ARCHIVE/source/guacamole-server-$VER.tar.gz" && \
    tar -xzf guacamole-server-$VER.tar.gz && \
    cd guacamole-server-*/ && \
    ./configure --with-init-dir=/etc/init.d && \
    make && \
    make install && \
    ldconfig && \
    systemctl enable guacd && \
    cd .. && \
    rm -rf guacamole-server-$VER/ && \
    rm guacamole-server-$VER.tar.gz

### install guacamole client
RUN wget "$GUAC_ARCHIVE/binary/guacamole-$VER.war" && \
    mkdir -p /etc/guacamole/ && \
    mv guacamole-$VER.war /etc/guacamole/guacamole.war && \
    ln -sf /etc/guacamole/guacamole.war /var/lib/tomcat9/webapps/

### install mariadb/mysql
RUN apt install --yes \
            mariadb-server mariadb-client mariadb-common libmariadb-java \
            libmysql-java && \
    mkdir -p /etc/guacamole/lib && \
    ln -sf /usr/share/java/mysql-connector-java.jar /etc/guacamole/lib/
    # ln -sf /usr/share/java/mariadb-java-client.jar /etc/guacamole/lib/
    # see: http://mail-archives.apache.org/mod_mbox/guacamole-user/201906.mbox/%3cCALKeL-OoiFctjKQHOcb8NbGseXMU-ed0xF0rm8aN+KPcMazCSw@mail.gmail.com%3e

### install the auth-jdbc extension
RUN wget "$GUAC_ARCHIVE/binary/guacamole-auth-jdbc-$VER.tar.gz" && \
    tar -xzf guacamole-auth-jdbc-$VER.tar.gz && \
    mkdir -p /etc/guacamole/extensions/ && \
    cp guacamole-auth-jdbc-$VER/mysql/guacamole-auth-jdbc-mysql-$VER.jar /etc/guacamole/extensions/ && \
    cp guacamole-auth-jdbc-$VER/mysql/schema/*.sql /etc/guacamole/ && \
    rm guacamole-auth-jdbc-$VER.tar.gz && \
    rm -rf guacamole-auth-jdbc-$VER/

RUN apt install --yes php

### add /usr/games on the PATH
RUN echo 'export PATH=/usr/games:$PATH' >  /etc/profile.d/path.sh

RUN apt purge --yes flatpak && \
    apt autoremove --yes

### install xscreensaver
RUN apt install --yes xscreensaver xscreensaver-data xscreensaver-data-extra

### install additional packages
sinclude(packages)
