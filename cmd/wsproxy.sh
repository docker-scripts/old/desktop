rename_function cmd_wsproxy orig_cmd_wsproxy
cmd_wsproxy() {
    orig_cmd_wsproxy "$@"
    [[ $1 == 'add' ]] &&  _customize_apache2_config
}

_customize_apache2_config() {
    # change apache2 configuration
    # see also: https://guacamole.apache.org/doc/gug/proxying-guacamole.html#apache
    cat <<EOF > $CONTAINERS/wsproxy/sites-available/$DOMAIN.conf
<VirtualHost *:80>
        ServerName $DOMAIN
        ProxyPass /.well-known/acme-challenge !
        Redirect permanent / https://$DOMAIN/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $DOMAIN
        ProxyPreserveHost On

        ProxyPass / https://$DOMAIN/ flushpackets=on
        ProxyPassReverse / https://$DOMAIN/

        <Location /websocket-tunnel>
            Require all granted
            ProxyPass ws://$DOMAIN/websocket-tunnel
            ProxyPassReverse ws://$DOMAIN/websocket-tunnel
        </Location>

        SetEnvIf Request_URI "^/tunnel" dontlog
        CustomLog  /var/log/apache2/guacamole.log common env=!dontlog

        ProxyRequests off

        SSLEngine on
        SSLCertificateFile      /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile   /etc/ssl/private/ssl-cert-snakeoil.key
        #SSLCertificateChainFile /etc/ssl/certs/ssl-cert-snakeoil.pem

        SSLProxyEngine on
        SSLProxyVerify none
        SSLProxyCheckPeerCN off
        SSLProxyCheckPeerName off
        SSLProxyCheckPeerExpire off
</VirtualHost>
EOF
    # reload the new configuration
    ds @wsproxy reload
}
