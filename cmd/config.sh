cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    check_settings

    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh

    # get a letencrypt ssl-cert
    ds wsproxy ssl-cert

    # create and setup user accounts
    ds inject setup-user-accounts.sh
    [[ -f accounts.txt ]] || cp $APPDIR/misc/accounts.txt .
	ds users create accounts.txt
    ds inject create-admin-user.sh

    # install and config epoptes
    ds inject install-epoptes.sh

    # install simple-chat
    ds exec apt install --yes php
    docker cp -a $APPDIR/misc/chat/ $CONTAINER:/var/www/html/
    ds exec chown www-data:www-data /var/www/html/chat/chat.txt
    ds exec systemctl restart apache2

    # install and setup guacamole
    ds inject guacamole.sh
}

fail() { echo "$@" >&2; exit 1; }

check_settings() {
    [[ $ADMIN_PASS == 'pass' ]] \
	&& fail "Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons."
    [[ $GUAC_PASS == 'pass' ]] \
	&& fail "Error: GUAC_PASS on 'settings.sh' has to be changed for security reasons."
}
