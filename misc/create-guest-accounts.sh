#!/bin/bash -x
### Create and config guest accounts.

guest=${1:-guest}
pass=${2:-pass}
count=${3:-30}

# create the template/skeleton guest account
groupadd guest --gid=500 -f
adduser $guest --uid=500 --gid=500 \
        --shell=/bin/bash --gecos '' \
        --disabled-password
usermod $guest --password="$(openssl passwd -stdin <<< $pass)"
chown $guest:guest -R /home/$guest

# create the guest accounts
rm -rf /home/guest-accounts/
mkdir -p /home/guest-accounts/
for i in $(seq 1 $count); do
    user="${guest}${i}"
    adduser $user --uid=$((500 + $i)) --gid=500 \
            --home=/home/guest-accounts/$user \
            --shell=/bin/bash --gecos '' \
            --disabled-password
    usermod $user --password="$(openssl passwd -stdin <<< $user)"
done

# create a script that resets a guest account
cat <<EOF > /usr/local/bin/reset-guest-account.sh
#!/bin/bash
[[ \$HOME =~ ^'/home/guest-accounts/$guest'[0-9]{1,3}\$ ]] || exit 1
cd \$HOME || exit 2
rm -rf .* *
rsync -a /home/$guest/ .
chown \$USER -R .
EOF
chmod +x /usr/local/bin/reset-guest-account.sh

# make sure guest accounts are reset on login and on logout
cat <<EOF > /etc/lightdm/lightdm.conf.d/reset-guest-accounts.conf
[SeatDefaults]
session-setup-script=/usr/local/bin/reset-guest-account.sh
session-cleanup-script=/usr/local/bin/reset-guest-account.sh
EOF
cat <<EOF > /etc/profile.d/reset-guest-account.sh
#!/bin/bash
/usr/local/bin/reset-guest-account.sh
EOF

### place some limits on guest accounts
sed -i /etc/security/limits.conf -e '/guest/d'
cat <<EOF >> /etc/security/limits.conf
### guest
@guest        hard    nproc           1000
@guest        hard    cpu             2
@guest        hard    maxlogins       1
EOF
