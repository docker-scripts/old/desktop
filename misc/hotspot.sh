#!/bin/bash

usage() {
    cat <<EOF
Usage: $0 [ start [<ssid>] [<pass>] | stop ]

If no <ssid> is given, the hostname will be used by default.
The default <pass> is '12345678'.

Example: $0 start "My Hotspot" pass-123

EOF
}

stop() {
    systemctl restart network-manager
}

start() {
    local ssid=${1:-$(hostname -s)}
    local pass=${2:-12345678}
    local ifname=$(lshw -quiet -c network | sed -n -e '/Wireless interface/,+12 p' | sed -n -e '/logical name:/p' | cut -d: -f2 | sed -e 's/ //g')

    # start the hotspot with the network-manager-cli
    echo "Starting hotspot with ssid='$ssid' and password='$pass'"
    nmcli dev wifi hotspot ifname "$ifname" ssid "$ssid" password "$pass"
}

main() {
    local cmd=$1 ; shift
    case $cmd in
        start) start "$@" ;;
        stop)  stop ;;
        *)     usage ;;
    esac
}

# start the main function
main "$@"
