#!/bin/bash -x

### Uncomment to change the default values for VNC.
#WS_PORT=6901        # websocket port for WebVNC
#VNC_PASS="pass"
#VNC_PORT="5901"
#VNC_WIDTH="1024"
#VNC_HEIGHT="768"
#VNC_DEPTH="24"

main() {
    install packages
    setup_vnc_server_and_lightdm
    setup_novnc_service
    setup_apache2
    #apache2_small_ram_config
}

install_packages() {
    # install vnc server and noVNC
    apt install --yes \
	novnc websockify net-tools python-numpy \
	vnc4server apache2
    # libvncserver-dev 

}

setup_vnc_server_and_lightdm() {
    # set vnc password
    local vnc_command
    if [[ -z $VNC_PASS ]]; then
	vnc_command='Xvnc -SecurityTypes None'
    else
	echo "$VNC_PASS" | vncpasswd -f >> /etc/vncpasswd
	chmod 600 /etc/vncpasswd
	vnc_command='Xvnc -SecurityTypes VncAuth -PasswordFile /etc/vncpasswd'
    fi

    # enable remote display and VNC server
    cat <<EOF > /etc/lightdm/lightdm.conf.d/20-vnc.conf
[XDMCPServer]
enabled=true

[VNCServer]
enabled=true
command=$vnc_command
port=${VNC_PORT:-5901}
width=${VNC_WIDTH:-1024}
height=${VNC_HEIGHT:-768}
depth=${VNC_DEPTH:-24}
EOF

    systemctl restart lightdm
}

setup_novnc_service() {
    # start noVNC service
    cat <<EOF > /etc/systemd/system/websockify.service
[Unit]
Description = start noVNC service
After = syslog.target network.target

[Service]
ExecStart = /usr/bin/websockify --web=/usr/share/novnc/ ${WS_PORT:-6901} localhost:${VNC_PORT:-5901}

[Install]
WantedBy = graphical.target
EOF
    systemctl enable websockify.service
    systemctl start websockify.service

    # open vnc.html by default
    ln -s /usr/share/novnc/{vnc,index}.html
}

setup_apache2() {
    local port=${WS_PORT:-6901}
    cat <<EOF > /etc/apache2/conf-available/webvnc.conf
<Location /vnc/>
    Require all granted
    ProxyPass http://127.0.0.1:$port/ flushpackets=on
    ProxyPassReverse http://127.0.0.1:$port/
    ProxyPassReverseCookiePath / /vnc/
</Location>

<Location /websockify>
    Require all granted
    ProxyPass ws://127.0.0.1:$port/websockify retry=3
    ProxyPassReverse ws://127.0.0.1:$port/websockify
</Location>
EOF

    a2enmod ssl proxy proxy_http proxy_wstunnel
    a2enconf webvnc.conf

    systemctl restart apache2
}

apache2_small_ram_config() {
    cat <<EOF > /etc/apache2/mods-available/mpm_event.conf
<IfModule mpm_event_module>
    StartServers              1
    MinSpareThreads          13
    MaxSpareThreads          45
    ThreadLimit              32
    ThreadsPerChild          13
    MaxRequestWorkers        80
    MaxConnectionsPerChild    0
</IfModule>
EOF
    systemctl restart apache2
}

# call the main function
main
