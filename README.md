# Note: This project is deprecated and is replaced by https://gitlab.com/docker-scripts/linuxmint and https://gitlab.com/docker-scripts/guacamole

# Linux Desktop In a Container

## Installation

### Making The Container

- First install `ds` and `wsproxy`:
   - https://gitlab.com/docker-scripts/ds#installation
   - https://gitlab.com/docker-scripts/wsproxy#installation

- Then get the scripts: `ds pull desktop`

- Create a dir for the container: `ds init desktop @desk.example.org`

- Fix the settings: `cd /var/ds/desk.example.org/; vim settings.sh`

- Make the container: `ds make`

- Open https://desk.example.org/ (or https://123.45.67.89:444/, where
  **444** is the **GUAC_PORT** on `settings.sh`).
    
### Installing Additional Packages

You can go inside the docker container and install other packages with
`apt`, like this:
```
cd /var/ds/desk.example.org/
ds shell
apt install ubuntu-edu-preschool ubuntu-edu-primary firefox
exit
```

However, in case you rebuild the server (for example with `ds
remake`), you will have to install them again manually. To install
them automatically on each rebuild, create a file like
`/var/ds/desk.example.org/packages` with a content like this:
```
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes \
        ubuntu-edu-preschool \
        ubuntu-edu-primary \
        firefox
```

See also
[packages.sample](https://gitlab.com/docker-scripts/desktop/blob/master/packages.sample).

### Accessing It From a Wi-Fi LAN

If the container is in a laptop and there is no wi-fi access point
around to enable the connection/communication with other laptops,
we can start a hotspot like this:
```
ds hotspot start test-hotspot pass1234
```

Afterwards the server can be accessed on https://10.42.0.1:444/

**Note:** If you have a firewall, make sure that the port **444** is
allowed. For example like this: `ufw allow 444`.

**Note:** Depending on the type of the wireless card or the driver
that it has, the hotspot may not always work. In this case the most
easy way to create a hotspot is with an Android or iPhone.


## Usage

### Managing User Accounts

The file `accounts.txt` contains a list of accounts in the form
`username:password`, which are created automatically when the container
is created. But they can also be re-created any time with a command like this:
```
ds users create accounts.txt
```

There are other commands and options which can be used to **export**,
**import**, **backup** and **restore** user accounts, for example:
```
ds users backup
ds users restore backup/users-20190417.tgz
```

See also: `ds inject users.sh help`

### Other Commands

```
ds help
ds stop
ds start
ds shell
ds remake
```

### Using Guacamole

[Guacamole](https://guacamole.apache.org/doc/gug/using-guacamole.html)
allows desktop access to the server from a web browser. Everything
runs on the server and the client does not consume any resources,
except for a browser tab. This is very convenient because:
- It does not require installation of any additional tools or
  applications on the client, except a web browser.
- It is cross-platform and universal, the desktop can be accessed from
  any system, on any architecture.

#### Accessing User Account From a Browser

Any user that has an account on the server can access the desktop of
the server from a web browser. He can do it like this:

- Open `https://10.42.0.1:444/` (replace `10.42.0.1` with the IP or
  name of the server).
- Click on the button **Login** without a username and password.
- Click on the **RDP** connection.
- Give the username and password of the account, for example
  (user1,pass1).

#### Teacher Sharing His Screen With The Students

This is useful when the teacher wants to demonstrate to the students
how to do something, for example how to write a program.

To be able to share the screen of his account, the teacher should
enter in Guacamole with the username `student` or `admin` (and the
corresponding password).

Then he should go to his account by clicking on the RDP connection and
giving the username and password of his account. Afterwards he should
follow these steps:

- Press **Ctrl+Alt+Shift** to open the Guacamole panel on the left
  side.
- Click on the **Share** menu of the top and then on **Watch**.
- Right-click on the link that is displayed and copy it.
- Open `https://10.42.0.1:444/chat/` in another tab and share the link
  with the students.
- Once the students open in browser the link of the shared Guacamole
  session, they will be able to see the desktop of the teacher and
  what he is doing.

#### Students Collaborating With The Teacher And With Each-Other

The students can share their desktop too with the teacher and with
other students. This is useful when a student is stuck and needs some
help (for example he cannot find a bug in the program) or when the
students are working in pairs or in groups.

To be able to share his desktop a student should login in Guacamole
with the username `student`. Then he should follow the same steps as
above, except that he should select **Collaborate** from the menu of
**Share**, instead of **Watch**. The difference is that with
**Collaborate** both parties (or all of them) can use the keyboard and
mouse to work together, instead of just watching what the other person
does.

Then they can use `https://10.42.0.1:444/chat/` to share the link of
the guacamole session with the others (or maybe use email or other
communication means).

#### Sharing Files With Each-Other

On the Guacamole left panel (Ctrl+Alt+Shift) click on **Shared
Drive**. This is a shared directory on the server where everyone can
upload and download files. This may be useful for distributing lecture
materials, for collecting homeworks, etc.
