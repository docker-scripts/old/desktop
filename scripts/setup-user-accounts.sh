#!/bin/bash -x

set -e
#source /host/settings.sh

# main function
main() {
    # setup configuration of the new accounts
    setup_configuration_of_new_accounts

    # enable password authentication
    sed -i /etc/ssh/sshd_config \
        -e "/PasswordAuthentication no\$/ c PasswordAuthentication yes"
    systemctl restart ssh
}

setup_configuration_of_new_accounts() {
    # create a group for student accounts
    [[ -z $(getent group student) ]] && addgroup student --gid=990

    # make new accounts belong to the student group
    sed -i /etc/adduser.conf \
	-e '/^USERGROUPS=/ c USERGROUPS=no' \
	-e '/^USERS_GID=/ c USERS_GID=990' \
	-e '/^DIR_MODE=/ c DIR_MODE=0700'

    # customize .bashrc of new accounts
    cat <<'EOF' > /usr/local/sbin/adduser.local
#!/bin/bash
user_home=$4
sed -i $user_home/.bashrc -e '/^#force_color_prompt=/c force_color_prompt=yes'
EOF
    chmod +x /usr/local/sbin/adduser.local

    # place some resource limits
    sed -i /etc/security/limits.conf -e '/student/d'
    cat <<EOF >> /etc/security/limits.conf
### student
@student        hard    nproc           10000
@student        hard    cpu             2
@student        hard    maxlogins       3
EOF

    # autostart xscreensaver on login
    cat <<EOF > /etc/xdg/autostart/xscreensaver.desktop
[Desktop Entry]
Type=Application
Exec=xscreensaver
Hidden=false
X-MATE-Autostart-enabled=true
Name[C]=xscreensaver
Name=xscreensaver
Comment[C]=
Comment=
X-MATE-Autostart-Delay=0
EOF
    # set default xscreensaver options
    cat <<EOF > /etc/skel/.xscreensaver
splash:         False
lock:           False
mode:           random
timeout:        0:05:00
cycle:          0:03:00
EOF
}

# call the main function
main
