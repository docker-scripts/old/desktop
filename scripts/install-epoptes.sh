#!/bin/bash -x

source /host/settings.sh

[[ -z $EPOPTES_USERS ]] && exit

apt install --yes epoptes

### add users to the group 'epoptes'
for user in $EPOPTES_USERS; do
    adduser $user epoptes
done

### fix the default client configuration
sed -i /etc/default/epoptes-client \
    -e 's/^WOL/#WOL/' \
    -e "s/^#\?SERVER.*/SERVER=$DOMAIN/"

### enable the service
systemctl enable epoptes
systemctl start epoptes
