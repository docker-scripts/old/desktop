#!/bin/bash -x

source /host/settings.sh

[[ -n $ADMIN_USER ]] || exit

user="$ADMIN_USER"

# create
delgroup $user 2>/dev/null
adduser $user --gecos '' --disabled-password
usermod $user --password "$(openssl passwd -stdin <<< $ADMIN_PASS)"
chown $user:$user -R /home/$user

# allow to use sudo without password
echo "$user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$user
chmod 0440 /etc/sudoers.d/$user
echo "alias sudo='sudo -h 127.0.0.1'" >> /home/$user/.bash_aliases

# set color prompt
sed -i /home/$user/.bashrc -e '/^#force_color_prompt=/c force_color_prompt=yes'
