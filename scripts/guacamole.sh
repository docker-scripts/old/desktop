#!/bin/bash -x
### Install guacamole:
### https://guacamole.apache.org/doc/gug/installing-guacamole.html

set -e   # stop on error

source /host/settings.sh

release=1.1.0

main() {
    local mysql_pass=$(random_pass)

    create_guacamole_db $mysql_pass
    setup_guacamole_db
    config_guacamole_properties $mysql_pass
    config_user_mapping

    setup_xrdp
    setup_apache
    #ram_optimizations
}

random_pass() {
    local size=${1:-16}
    echo -n $(tr -cd '[:alnum:]' < /dev/urandom | fold -w$size | head -n1)
}

create_guacamole_db() {
    local mysql_pass=$1

    # create the database and user
    mysql -e "
        DROP DATABASE IF EXISTS guacamole_db;
        CREATE DATABASE guacamole_db;
        DROP USER IF EXISTS 'guacamole_user'@'localhost';
        CREATE USER 'guacamole_user'@'localhost' IDENTIFIED BY '$mysql_pass';
        GRANT SELECT,INSERT,UPDATE,DELETE ON guacamole_db.* TO 'guacamole_user'@'localhost';
        FLUSH PRIVILEGES;
    "

    # create the db tables
    cat /etc/guacamole/*.sql | mysql guacamole_db
    rm /etc/guacamole/*.sql

    # change the default password of guacadmin
    [[ -n $GUAC_PASS ]] && mysql guacamole_db -e "
        SET @salt = UNHEX(SHA2(UUID(), 256));

        UPDATE guacamole_user
        SET password_salt = @salt,
            password_hash = UNHEX(SHA2(CONCAT('$GUAC_PASS', HEX(@salt)), 256))
        WHERE user_id = 1;

        UPDATE guacamole_entity
        SET name = '$GUAC_ADMIN'
        WHERE entity_id = 1 AND type = 'USER';
        "
}

setup_guacamole_db() {
    # create connections
    mysql guacamole_db -e "
        INSERT INTO guacamole_connection
            (connection_name, protocol)
        VALUES
            ('RDP', 'rdp'),
            ('SSH', 'ssh');

        INSERT INTO guacamole_connection_parameter
        VALUES
            (1, 'create-drive-path', 'true'),
            (1, 'drive-name', 'shared'),
            (1, 'drive-path', '/host/shared'),
            (1, 'enable-drive', 'true'),
            (1, 'hostname', '127.0.0.1'),
            (2, 'hostname', '127.0.0.1');

        INSERT INTO guacamole_sharing_profile
            (sharing_profile_name, primary_connection_id)
        VALUES
            ('Watch', 1),
            ('Watch', 2),
            ('Collaborate', 1),
            ('Collaborate', 2);

        INSERT INTO guacamole_sharing_profile_parameter
        VALUES
            (1, 'read-only', 'true'),
            (2, 'read-only', 'true');
    "

    # create a user
    [[ -n $GUAC_USER_NAME ]] || return
    mysql guacamole_db -e "
        INSERT INTO guacamole_entity
        SET type = 'USER',
            name = '$GUAC_USER_NAME';

        SET @salt = UNHEX(SHA2(UUID(), 256));
        INSERT INTO guacamole_user
        SET entity_id = 2,
            password_hash = UNHEX(SHA2(CONCAT('$GUAC_USER_PASS', HEX(@salt)), 256)),
            password_salt = @salt,
            password_date = NOW();

        INSERT INTO guacamole_user_permission
        VALUES
            (2,2,'READ');

        INSERT INTO guacamole_connection_permission
        VALUES
            (2,1,'READ'),
            (2,2,'READ');

        INSERT INTO guacamole_sharing_profile_permission
        VALUES
            (2,1,'READ'),
            (2,2,'READ'),
            (2,3,'READ'),
            (2,4,'READ');
    "
}

config_guacamole_properties() {
    local mysql_pass=$1
    local max_connections=${GUAC_MAX_CONNECTIONS:-50}

    mkdir -p /etc/guacamole
    cat <<EOF > /etc/guacamole/guacamole.properties
guacd-hostname: localhost
guacd-port:     4822

mysql-hostname: localhost
mysql-port:     3306
mysql-database: guacamole_db
mysql-username: guacamole_user
mysql-password: $mysql_pass
mysql-default-max-connections: $max_connections
mysql-default-max-connections-per-user: $max_connections

auth-provider:  net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
user-mapping:   /etc/guacamole/user-mapping.xml
EOF

    # link to tomcat libraries
    ln -sf /etc/guacamole/guacamole.properties \
       /usr/share/tomcat9/lib/guacamole.properties

    systemctl restart tomcat9
}

config_user_mapping() {
    cat <<EOF > /etc/guacamole/user-mapping.xml
<user-mapping>
    <authorize username="" password="">
        <connection name="RDP">
            <protocol>rdp</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="enable-audio-input">true</param>
            <param name="enable-printing">true</param>
        </connection>
        <connection name="SSH">
            <protocol>ssh</protocol>
            <param name="hostname">127.0.0.1</param>
            <param name="enable-sftp">true</param>
            <param name="font-name">Monaco</param>
        </connection>
    </authorize>
</user-mapping>
EOF
    # fix permissions and ownership
    chmod 600 /etc/guacamole/user-mapping.xml
    chown tomcat:tomcat /etc/guacamole/user-mapping.xml

    install_font_monaco
}

install_font_monaco() {
    local font_dir=/usr/share/fonts/truetype/Monaco
    [[ -d $font_dir ]] && return

    mkdir -p $font_dir
    cd $font_dir
    wget https://github.com/mikesmullin/gvim/raw/master/.fonts/Monaco_Linux.ttf
    fc-cache -f .
    cd -
}

setup_xrdp() {
    apt install --yes \
        xrdp xorgxrdp xrdp-pulseaudio-installer
    sed -i /etc/xrdp/xrdp.ini \
        -e '/^\[console\]/,$ s/^/# /' \
        -e '/^\[X11rdp\]/,+8 s/^/# /' \
        -e '/^\[Xvnc\]/,+8 s/^/# /'

    install_pulseaudio_module_xrdp

    systemctl restart xrdp
}

# install pulseaudio-module-xrdp
# https://github.com/neutrinolabs/pulseaudio-module-xrdp/wiki/README
install_pulseaudio_module_xrdp() {
    # make sure that we have the needed tools
    apt install --yes pulseaudio build-essential dpkg-dev git

    # get the source of pulseaudio
    cat <<EOF > /etc/apt/sources.list.d/bionic-updates-src.list
deb-src http://archive.ubuntu.com/ubuntu bionic-updates main restricted universe multiverse
EOF
    apt update
    apt source pulseaudio

    # build the pulseaudio package
    apt build-dep --yes pulseaudio
    cd pulseaudio-11.1
    ./configure
    cd ..

    # build xrdp source / sink modules
    git clone https://github.com/neutrinolabs/pulseaudio-module-xrdp.git
    cd pulseaudio-module-xrdp
    ./bootstrap && ./configure PULSE_DIR=$(dirname $(pwd))/pulseaudio-11.1
    make
    make install
    cd ..

    # cleanup
    rm -rf pulseaudio*
    rm /usr/lib/pulse-11.1/modules/*.la
    rm /etc/apt/sources.list.d/bionic-updates-src.list
}

setup_apache() {
    apt install --yes apache2

    # based on: https://guacamole.apache.org/doc/gug/proxying-guacamole.html#apache
    cat <<EOF > /etc/apache2/conf-available/guacamole.conf
ProxyPass /chat !

ProxyPass / http://localhost:8080/guacamole/ flushpackets=on
ProxyPassReverse / http://localhost:8080/guacamole/
ProxyPassReverseCookiePath /guacamole/ /

<Location /websocket-tunnel>
    Require all granted
    ProxyPass ws://localhost:8080/guacamole/websocket-tunnel
    ProxyPassReverse ws://localhost:8080/guacamole/websocket-tunnel
</Location>

SetEnvIf Request_URI "^/tunnel" dontlog
CustomLog  /var/log/apache2/guacamole.log common env=!dontlog
EOF

    a2enmod ssl proxy proxy_http proxy_wstunnel
    a2ensite default-ssl.conf
    a2enconf guacamole.conf

    # comment CustomLog on 'default-ssl.conf'
    sed -i /etc/apache2/sites-enabled/default-ssl.conf \
        -e 's/CustomLog/#CustomLog/'

    systemctl reload apache2
}

ram_optimizations() {

    # apache2 small ram configuration
    cat <<EOF > /etc/apache2/mods-available/mpm_event.conf
<IfModule mpm_event_module>
    StartServers              1
    MinSpareThreads          13
    MaxSpareThreads          45
    ThreadLimit              32
    ThreadsPerChild          13
    MaxRequestWorkers        80
    MaxConnectionsPerChild    0
</IfModule>
EOF
    systemctl restart apache2

    # tomcat
    sed -i /etc/default/tomcat9 \
        -e '/^JAVA_OPTS/ a JAVA_OPTS="${JAVA_OPTS} -Xmx20m -Xms20m"'
    systemctl restart tomcat9
}

# call the main function
main
